package com.example.ko_theprogrammer.converter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button exitButton = (Button) findViewById(R.id.exitButton);
        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.exit(0);
            }
        });

        Button convertButton = (Button) findViewById(R.id.convertButton);
        convertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText millimeter = (EditText) findViewById(R.id.mmEditText);
                EditText inches = (EditText) findViewById(R.id.inEditText);

                double result, mm;
                mm = Double.parseDouble(millimeter.getText().toString());
                result = (mm/25.4);

                inches.setText(Double.toString(result));
            }
        });
    }
}
